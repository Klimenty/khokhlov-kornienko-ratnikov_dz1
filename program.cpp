#include <iostream>
using namespace std;
#include <string>
#include<windows.h>
#include <conio.h>
#include <ctime>

using namespace std;

void ChangeColor(string words)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(handle, 0x0004);
	cout << words;
	SetConsoleTextAttribute(handle, 0x0007);
}
string SortSentence(string sentence)
{
	string outputSentence;
	int i = 0;

	while (sentence[i] == ' ')
	{
		i++;
	}
	while ((i < size(sentence)) and (sentence[i] != '.') and (sentence[i] != '!') and (sentence[i] != '?'))
	{
		if (not ((sentence[i] == ' ') and (sentence[i + 1] == ' ') or ((sentence[i] == ' ') and ((sentence[i + 1] == '.') or (sentence[i + 1] == '!') or (sentence[i + 1] == '?')))))
		{
			outputSentence += sentence[i];
		}
		i++;
	}
	outputSentence += sentence[i];
	return outputSentence;
}
string ExchangeWords(string words) // Процедура замены местами слов в предложении
{
	//setlocale(0, "");
	string word1; // 1-ое слово
	string word2; // 2-ое слово
	string buf;
	string result; // результат
	string sentence; // предложение из текста
	char sign; // знак в конце предложения
	int i = 0; // счетчик позиции символа в тексте
	int indicator1, indicator2;
	int countSentence = 0; // кол-во предложений
	int countWords = 0; // кол-во слов в предложении

	sentence = SortSentence(words);
	for (int j = 0; j < size(sentence); j++)
	{
		if ((sentence[j] == ' ') or (sentence[j] == '.') or ((sentence[j] == '!') and (sentence[j - 1] != ' ')) or ((sentence[j] == '?') and sentence[j - 1] != ' '))
		{
			countWords++;
		}
	}
	if (countWords > 1)
	{
		for (int j = 1; j < ((countWords / 2) + 1); j++)
		{
			word1 = "";
			word2 = "";
			buf = "";
			indicator1 = 0;
			indicator2 = 0;
			while ((sentence[i] != ' ') and (sentence[i] != '.') and (sentence[i] != '!') and (sentence[i] != '?'))
			{
				word1 += sentence[i];
				i++;
			}
			while (sentence[i] == ' ')
			{
				i++;
			}
			while ((sentence[i] != ' ') and (sentence[i] != '.') and (sentence[i] != '!') and (sentence[i] != '?'))
			{
				word2 += sentence[i];
				i++;
			}
			while (sentence[i] == ' ')
			{
				i++;
			}


			if (word1[word1.length() - 1] == ',')
			{
				indicator1++;
			}

			if (word2[word2.length() - 1] == ',')
			{
				indicator2++;
			}

			if (indicator1 != indicator2)
			{
				if (word1[word1.length() - 1] == ',')
				{
					for (int k = 0; k < (word1.length() - 1); k++)
					{
						buf += word1[k];
					}
					word1 = "";
					word1 += buf;
					word2 += ',';
					buf = "";
				}
				else
				{
					for (int k = 0; k < (word2.length() - 1); k++)
					{
						buf += word2[k];
					}
					word2 = "";
					word2 += buf;
					word1 += ',';
				}
			}

			if (j < ((countWords / 2)))
			{

				result += word2 + ' ' + word1 + ' ';
			}
			else
			{
				result += word2 + ' ' + word1;
			}
		}
		while (sentence[i] == ' ')
		{
			i++;
		}
		if (countWords % 2 != 0)
		{
			result += ' ';
			while (i < size(sentence) - 2)
			{
				result += sentence[i];
				i++;
			}
		}
		result += words[size(words) - 1];
		result = SortSentence(result);
    }
	else
	{
		result = sentence;
	}
	return result;
}
void ExchangeWordsFiveSentences(string words)
{
	string sentence;
	int i = 0;
	int countSentence = 0;
	while (i < size(words))
	{
		if ((words[i] != '.') and (words[i] != '!') and (words[i] != '?'))
		{
			sentence += words[i];
			i++;
		}
		else
			if (countSentence < 5)
			{
				countSentence++;
				sentence += words[i];
				sentence = SortSentence(sentence);
				cout << ExchangeWords(sentence) << " ";
				i++;
				sentence = "";
				while (words[i] == ' ')
				{
					i++;
				}
			}
			else
			{
				{
					countSentence++;
					sentence += words[i];
					sentence = SortSentence(sentence);
					cout << sentence;
					i++;
					sentence = "";
					while (words[i] == ' ')
					{
						i++;
					}
				}
			}
	}
}
void Allotment(string words)
{
	int countSentences = 0;
	int numberSentence = 0;
	char sym = ' ';
	string sentence;

	for (int i = 0; i <= size(words); i++)
	{
		if ((words[i] == '.') or (words[i] == '!') or (words[i] == '?'))
		{
			countSentences++;
		}
	}

	int i = 0;

	string *sentences = new string[countSentences + 1];
	while (i < size(words))
	{
		if ((words[i] != '.') and (words[i] != '!') and (words[i] != '?'))
		{
			sentence += words[i];
			i++;
		}
		else
		{
			numberSentence++;;
			sentence += words[i];
			sentence = SortSentence(sentence);
			sentences[numberSentence] = sentence;
			sentence = "";
			i++;
			while (words[i] == ' ')
			{
				i++;
			}
		}
	}
	numberSentence = 1;
	do
	{
		for (int i = 1; i <= countSentences; i++)
		{
			if (i == numberSentence)
			{
				ChangeColor(sentences[i]);
				cout << ' ';
			}
			else
			{
				cout << sentences[i];
				cout << ' ';
			}
		}
		cout << "\nКол-во предложений: " << countSentences << endl;
		sym = _getch();
		if ((sym == 'd') or (sym == 'D'))
		{
			numberSentence++;
		}
		if ((sym == 'a') or (sym == 'A'))
		{
			numberSentence--;
		}
		if (numberSentence == countSentences + 1)
		{
			numberSentence = 1;
		}
		if (numberSentence == 0)
		{
			numberSentence = countSentences;
		}
		system("cls");
	} while ((int)sym != 27);
	//cout << countSentences;
}
void Ex2(string words)
{
	int sign = 0;
	int j = 0;
	int countSentences = 0;
	string sentence;
	string word;
	int countWords = 0;

	system("cls");
	while (j < size(words))
	{
		if ((words[j] == '.') or (words[j] == '!') or (words[j] == '?'))
		{
			countSentences++;
		}
		j++;
	}
	if (countSentences < 2)
	{
		cout << "Введино меньше двух проедложений." << endl;
	}
	else
	{
		for (int i = 0; i <= size(words); i++)
		{
			if (sign == 1)
			{
				sentence += words[i];
			}
			if ((words[i] == '!') or (words[i] == '.') or (words[i] == '?'))
			{
				sign++;
			}
		}
		sentence = SortSentence(sentence);
		j = 0;
		for (int i = 0; i <= size(sentence); i++)
		{
			if ((sentence[i] == '!') or (sentence[i] == '.') or (sentence[i] == '?') or (sentence[i] == ' '))
			{
				countWords++;
			}
		}
		cout << "Второе предложение: " << sentence << endl;
		cout << "Четные слова во втором предложении: ";
		for (int i = 1; i <= countWords; i++)
		{
			word = "";
			while ((j <= size(sentence)) and (sentence[j] != '!') and (sentence[j] != '.') and (sentence[j] != '?') and (sentence[j] != ' '))
			{
				word += sentence[j];
				j++;
			}
			if (i % 2 == 0)
			{
				cout << word << " ";
			}
			j++;
		}
		cout << endl << "Общее кол-во слов во втором редложении: " << countWords << endl;
	}
}

void DecToBin(string s)
{
	system("cls");
	int i = 0, b, e, c, n = 0, answer = 0, numb = 0, ex = 0;
	while ((i < s.length()) && (ex == 0))
	{
		while ((((char)s[i] < 48) || ((char)s[i] > 57)) && (i < (s.length() - 1)))
		{
			i++;
		}

		b = i;

		do
		{
			i++;
		} while (((char)s[i] > 47) && ((char)s[i] < 58) && (i < s.length()));

		e = i - 1;

		if ((s[i] == ' ') || ((s[i] == ',') || (s[i] == '.') && (((char)s[i] < 48) || ((char)s[i] > 57))))
		{
			ex++;
		}
	}

	for (int j = e; j >= b; j--)
	{
		numb += ((int)s[j] - 48)*pow(10, n);
		n++;
	}

	if (s[b - 1] == '-')
	{
		numb *= -1;
	}

	cout << numb << endl;
	cout << "DONE.";
	system("pause");
}

void ChooseSentence(string text)
{
	int numb, countSentences = 0, b = 0;
	string sentence = "";

	for (int j = 0; j < size(text); j++)
	{
		if ((text[j] == '.') or (text[j] == '!') or (text[j] == '?'))
		{
			countSentences++;
		}
	}
	numb = countSentences + 2;

	do
	{
		cout << "Choose the number of sentence\n" << text << endl;
		cin >> numb;
	} while (numb > countSentences);

	numb--;
	countSentences = 0;

	for (; (b < size(text)) && (countSentences < numb); b++)
	{
		if ((text[b] == '.') or (text[b] == '!') or (text[b] == '?'))
		{
			countSentences++;
		}
	}

	while ((text[b] != '.') and (text[b] != '!') and (text[b] != '?'))
	{
		b++;
		sentence += text[b];
	}

	DecToBin(sentence);
	system("cls");
}

void ConsonantsInAlphabeticalOrder(string s)
{
	string buf;
	string mas;
	char ch;
	char destroy;
	int i = 0, c = 0;
	system("cls");
	while (i < s.length())
	{
		buf = "";
		c = 0;
		ch = s[i];
		switch (s[i])
		{
		case'b':case'c':case'd':case'f':case'g':case'h':case'j':case'k':case'l':case'm':
		case'n':case'p':case'q':case'r':case's':case't':case'v':case'w':case'x':case'z':
		case'B':case'C':case'D':case'F':case'G':case'H':case'J':case'K':case'L':case'M':
		case'N':case'P':case'Q':case'R':case'S':case'T':case'V':case'W':case'X':case'Z':
		{
			mas += s[i];
		}
		default:
		{
			break;
		}
		}


		for (i = 0; i < s.length(); i++)
		{
			if (s[i] != ch)
			{
				if ((((char)s[i] > 64) && ((char)s[i] < 91)) || ((((char)s[i] > 96) && ((char)s[i] < 123))))
				{
					buf += s[i];
				}
			}
		}
		s = buf;
		i = 0;
	}

	cout << "\nLower case letters:" << endl;
	for (int j = 63; j < 91; j++)
	{
		for (int k = 0; k < mas.length(); k++)
		{
			if ((char)mas[k] == j)
			{
				cout << mas[k] << " ";
			}
		}
	}

	cout << "\nUpper case letters:" << endl;
	for (int j = 97; j < 123; j++)
	{
		for (int k = 0; k < mas.length(); k++)
		{
			if ((char)mas[k] == j)
			{
				cout << mas[k] << " ";
			}
		}
	}
	cout << "DONE.";
	system("pause");
	system("cls");
}

int main()
{
	srand(time(NULL));

	char option;
	string word;
	int countWords = 0;
	int countSentence = 0;
	string sentence;

	setlocale(0, "");
	cout << "Please, enter the sentences: ";
	getline(cin, word);

	option = '0';
	do
	{
		cout << "\n1. Alternately highlight sentences and display the number of sentences in the text.";
		cout << "\n2. Displays the number of words in second sentence and it's every even words.";
		cout << "\n3. Convert the first number of a sentence from decimal to binary.";
		cout << "\n4. Print in alphabetical order all consonant letters included in this text.";
		cout << "\n5. In the first five sentences swaps every two adjacent words.";
		cout << "\nPress \"e\" to exit\n";
		option = _getch();
		system("cls");
		switch (option)
		{
			case '1':Allotment(word); break;
			case '2':Ex2(word); break;
			case '3':ChooseSentence(word); break;
			case '4':ConsonantsInAlphabeticalOrder(word); break;
			case '5':ExchangeWordsFiveSentences(word); break;
		}
		if ((option == 'e') or (option == 'E'))
		{
			break;
		}
		else
		{
			system("pause");
		}

	} while (true);
}